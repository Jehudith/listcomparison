# -*- coding: utf-8 -*-

"""
Created for Listcomparison
@date:      19/10/2016
@auhor:     Jehudith Dorfman
@version:   1.0

Made with PyCharm Community Edition
"""


class ListComparison(object):
    """
    Insert 2 different list and search for genes which appear in both lists
    """

    def __init__(self):
        super(ListComparison, self).__init__()

    def get_matching_genes(self, list1, list2):
        # use not in statement for unique changed gene
        # use only in statement to find a matched gene
        m1 = list(filter(lambda x: x not in list2, list1))
        m2 = list(filter(lambda x: x not in list1, list2))

        cross = m1.copy()
        cross.extend(m2)

        return m1, m2, set(cross)


    def get_input(self, input_text):
        print(input_text)
        lines = []
        text = ""
        while True:
            line = input()
            if line:
                lines.append(line)
            else:
                break
            text = '\n'.join(lines)
        return text

    def main(self):

        while True:
            cmd_start = input("command?, insert or exit ")
            if cmd_start == "exit":
                break

            if cmd_start == "insert":

                #Get first list input
                input_text = "Insert first list: \n"
                text = self.get_input(input_text)

                # Get genelist
                gene_list_set_1 = text.strip().split("\n")
                print("gene list", gene_list_set_1)

                #get second list input
                another_input = "insert second list \n"
                text = self.get_input(another_input)

                #Get genelist
                gene_list_set_2 = text.strip().split("\n")

                #Get the gene which appears in both lists
                m1, m2, matching = self.get_matching_genes(gene_list_set_1, gene_list_set_2)

                print("Not matching genes list1:\n")
                for gene in m1:
                    print("{0}".format(gene))
                print("\n")

                print("Not matching genes list2:\n")
                for gene in m2:
                    print("{0}".format(gene))
                print("\n")

                print("Not Matching genes cross list:\n")

                for gene in matching:
                    print("{0}".format(gene))
                print("\n")


if __name__ == "__main__":
    lc = ListComparison()
    lc.main()